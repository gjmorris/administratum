﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transport {

    public int number;

    public int sector;
    public int planet;

    public int menTotal = 0;
    public int mineralTotal = 0;
    public int suppliesTotal = 0;
    public int ceramiteTotal = 0;
    public int unitTotal = 0;

    public GameObject ship;

    public List<int> units = new List<int>();

    public int capacityTotal = 30;
    public int capacityCurrent;

    public Transport(int num, int sect, int plnt, GameObject ship) {

        number = num;
        sector = sect;
        planet = plnt;
        capacityCurrent = 0;

        this.ship = ship;

        this.ship.transform.position = GameController.gc.Sectors[sector , planet].transportPosition;
        this.ship.transform.rotation = Quaternion.Euler(GameController.gc.Sectors[sector , planet].transportRotation);
        ship.GetComponent<TransportListener>().Initialize(number);

        GameController.gc.Sectors[sector , planet].hasTransport = true;

    }

    public void SetPosition(Planet p) {

        this.ship.transform.position = p.transportPosition;
        this.ship.transform.rotation = Quaternion.Euler(p.transportRotation);

    }
}
