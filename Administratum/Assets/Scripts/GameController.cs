﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    /*
     * 0 for player 1 (sector with terra)
     *
     * 
     * 
     */

    public Planet[,] Sectors = new Planet[5, 5];
    public List<Transport> Transports = new List<Transport>();
    public List<Enemy> Enemies = new List<Enemy>();

    public static GameController gc;

    public GameObject Transport;
    public GameObject EnemyShip;

    public GameObject PlanetInfoBar;
    public GameObject TransportInfoBar;
    public GameObject RegimentInfoBar;
    public GameObject TranferMaterialsInfoBar;
    public GameObject TransferUnitsInfoBar;
    public GameObject[] RegimentButtons;
    public GameObject[] TransferUnitPlanetButtons;
    public GameObject[] TransferUnitTransButtons;
    public Text[] RegimentButtonsText;
    public Text[] TransferRegimentPlanetButtons;
    public Text[] TransferRegimentTransButtons;

    public Text PlanetNameTxt;
    public Text PlanetTypeTxt;
    public Text PlanetCooldownTxt;
    public Text TransportTxt;
    public Text PlayerTxt;
    public Text TurnTxt;

    public Text Lvl1MenTxt;
    public Text Lvl1MineralTxt;
    public Text Lvl1SuppliesTxt;
    public Text Lvl1CeramiteTxt;

    public Text Lvl2MenTxt;
    public Text Lvl2MineralTxt;
    public Text Lvl2SuppliesTxt;
    public Text Lvl2CeramiteTxt;

    public Text Lvl3MenTxt;
    public Text Lvl3MineralTxt;
    public Text Lvl3SuppliesTxt;
    public Text Lvl3CeramiteTxt;

    public Text TotalResourceTxt;
    public Text TotalMenTxt;
    public Text TotalMineralTxt;
    public Text TotalSuppliesTxt;
    public Text TotalCeramiteTxt;

    public Text RegimentsTxt;
    public Text UnitCapTotalTxt;
    public Text UnitStrengthTxt;

    public Text TransTotalTxt;
    public Text TransMenTxt;
    public Text TransMineralTxt;
    public Text TransSuppliesTxt;
    public Text TransCeramiteTxt;
    public Text TransUnitsTxt;
    public Text TransUnitPowerTxt;

    public Text TransferPlanetTxt;
    public Text TransferTransportTxt;
    public Text PlanetMenTxt;
    public Text TransportMenTxt;
    public Text PlanetMineralTxt;
    public Text TransportMineralTxt;
    public Text PlanetSuppliesTxt;
    public Text TransportSuppliesTxt;
    public Text PlanetCeramiteTxt;
    public Text TransportCeramiteTxt;
    public Text MatPlanetCapTxt;
    public Text MatTransCapTxt;

    public Text UnitTransPlanetTxt;
    public Text UnitTransTransTxt;
    public Text UnitPlanetSpaceTxt;
    public Text UnitTransSpaceTxt;

    public Text PlanetConqueredTxt;
    public Text PlanetConqueredBackgroundTxt;

    public Button ManufactorumBtn;
    public Button RegimentBtn;
    public Button Activate1Btn;
    public Button Activate2Btn;
    public Button Activate3Btn;

    public Button TransferMaterialBtn;
    public Button TransferUnitsBtn;

    public GameObject[] TravelButtons;

    public Text[] TravelButtonsTxt;

    public Sprite ManufactorumEnabled;
    public Sprite ManufactorumDisabled;

    int sector;
    int planet;

    int transport;

    int player;

    int enemy = 0;

    int turn = 0;

    private void Awake() {

        if (gc != null) {
            Destroy(gc);
        }
        gc = this;

        for (int i = 0; i < 5; i++) {
            Sectors[0, i] = new Planet();

        }
        Sectors[1 , 0] = new Planet();
        Sectors[1 , 1] = new Planet();

        player = 0;

        Sectors[0 , 0].Initialize("Terra", 8, new Vector3(-0.2f, 1.5f, -0.9f), new Vector3(0f, -45f, 0f));//terra
        Sectors[0 , 1].Initialize("Men", 5, new Vector3(-3.1f, 1.5f, -4.2f), new Vector3(0f, -200f, 0f));//blue
        Sectors[0 , 2].Initialize("Minerals", 5, new Vector3(-2.5f, 1.5f, 2f), new Vector3(0f,-70f, 0f));//red
        Sectors[0 , 3].Initialize("Supplies", 5, new Vector3(2.1f, 1.5f, -2.8f), new Vector3(0f,0f,0f));//green
        Sectors[0 , 4].Initialize("Balanced", 6, new Vector3(3.3f, 1.5f, 2.4f), new Vector3(0, 49f, 0f));//yellow
        Sectors[1 , 0].Initialize("enemy", 0, new Vector3(-5.4f, 1.5f, -6f) , new Vector3(-90f, 41f, 0f));//enemy south
        Sectors[1 , 1].Initialize("enemy", 0, new Vector3(5.5f, 1.5f, 0f), new Vector3(-90f, -107f, 0));//enemy north

        SetConnections();

        Transports.Add(new Transport(0, 0, 0, Instantiate(Transport)));
        Transports.Add(new Transport(1, 0, 1, Instantiate(Transport)));

        RollUnits();

    }

    private void Update() {

        if (Input.GetKey(KeyCode.Escape)) {
            PlanetInfoBar.SetActive(false);
            TransportInfoBar.SetActive(false);
            RegimentInfoBar.SetActive(false);
            TranferMaterialsInfoBar.SetActive(false);
            TransferUnitsInfoBar.SetActive(false);
        }

    }

    public void RollUnits() {//called at the beginning on a new player turn. ReRolls that player's planets' unitValues array

        for (int i = 0; i < 5; i++) {

            for (int j = 0; j < 10; j++) {

                int roll = Random.Range(0 , 10);

                if (roll == 0 || roll == 1) {
                    Sectors[player , i].unitValues[j] = 1;
                }
                else if (roll == 2 || roll == 3 || roll == 4 || roll == 5) {
                    Sectors[player , i].unitValues[j] = 2;
                }
                else if (roll == 6 || roll == 7 || roll == 8) {
                    Sectors[player , i].unitValues[j] = 3;
                }
                else {
                    Sectors[player , i].unitValues[j] = 4;
                }
            }
        }

    }

    public void SpawnEnemies()
    {
        int roll = Random.Range(0 , 10);

        if (turn > 5 && turn <= 10) {
            if (roll == 0) {
                enemy++;

                Enemy tempEnemy = new Enemy(enemy , 1 , roll % 2 , Instantiate(EnemyShip));
                tempEnemy.SetPosition(Sectors[1 , roll % 2]);
                tempEnemy.strength = Random.Range(10, 26);

                Enemies.Add(tempEnemy);

            }
        }
        else if (turn > 10 && turn <= 15) {
            if (roll >= 0 && roll < 3) {
                enemy++;

                Enemy tempEnemy = new Enemy(enemy , 1 , roll % 2 , Instantiate(EnemyShip));
                tempEnemy.SetPosition(Sectors[1 , roll % 2]);
                tempEnemy.strength = Random.Range(15 , 31);

                Enemies.Add(tempEnemy);
            }
        }
        else if (turn > 15 && turn <= 20) {
            if (roll >= 0 && roll < 5) {
                enemy++;

                Enemy tempEnemy = new Enemy(enemy , 1 , roll % 2 , Instantiate(EnemyShip));
                tempEnemy.SetPosition(Sectors[1 , roll % 2]);
                tempEnemy.strength = Random.Range(18 , 33);

                Enemies.Add(tempEnemy);
            }
        }
        else if (turn > 20) {
            if (roll >= 0 && roll < 7) {
                enemy++;

                Enemy tempEnemy = new Enemy(enemy , 1 , roll % 2 , Instantiate(EnemyShip));
                tempEnemy.SetPosition(Sectors[1 , roll % 2]);
                tempEnemy.strength = Random.Range(20 , 40);

                Enemies.Add(tempEnemy);
            }
        }


    }

    public void MoveEnemies() {

        foreach (Enemy enem in Enemies) {

            Planet currPlanet = Sectors[enem.sector , enem.planet];

            

        }

    }

    public void EndTurnButton()//End turn button in the bottom right
    {
        PlanetInfoBar.SetActive(false);
        TransportInfoBar.SetActive(false);
        RegimentInfoBar.SetActive(false);
        TranferMaterialsInfoBar.SetActive(false);
        TransferUnitsInfoBar.SetActive(false);

        //player++;
        turn++;

        TurnTxt.text = turn.ToString();

        //if (player > 4) {
            //player = 0;
        //}

        PlayerTxt.text = (player + 1).ToString();

        for (int i = 0; i < 5; i++)
        {
            Sectors[player, i].ReduceCooldown();
        }

        RollUnits();
        SpawnEnemies();

    }

    public void ManufactorumButton() {//button under PlayerBarInfo that activates the Mechanicum button

        print("Mechanicum yeet");

    }

    public void RegimentButton() {//button under PlayerBarInfo that sets the RegimentInfoBar visible

        for (int i = 0; i < 8; i++) {
            RegimentButtons[i].SetActive(false);
        }

        RegimentInfoBar.SetActive(true);

        for (int i = 0; i < Sectors[sector, planet].regiments; i++) {

            if (Sectors[sector, planet].unitValues[i] != -1) {

                RegimentButtons[i].SetActive(true);

                RegimentButtonsText[i].text = Sectors[player , planet].unitValues[i].ToString();
            }

        }

    }

    public void TransferMaterialButton() {
        Transport currTrans = Transports[transport];
        Planet currPlanet = Sectors[currTrans.sector , currTrans.planet];

        PlanetInfoBar.SetActive(false);
        TransportInfoBar.SetActive(false);
        RegimentInfoBar.SetActive(false);
        TranferMaterialsInfoBar.SetActive(true);
        TransferUnitsInfoBar.SetActive(false);

        TransferPlanetTxt.text = "(" + currTrans.sector + ", " + currTrans.planet + ")";
        TransferTransportTxt.text = currTrans.number.ToString();

        PlanetMenTxt.text = currPlanet.menTotal.ToString();
        TransportMenTxt.text = currTrans.menTotal.ToString();
        PlanetMineralTxt.text = currPlanet.mineralTotal.ToString();
        TransportMineralTxt.text = currTrans.mineralTotal.ToString();
        PlanetSuppliesTxt.text = currPlanet.suppliesTotal.ToString();
        TransportSuppliesTxt.text = currTrans.suppliesTotal.ToString();
        PlanetCeramiteTxt.text = currPlanet.ceramiteTotal.ToString();
        TransportCeramiteTxt.text = currTrans.ceramiteTotal.ToString();
        MatPlanetCapTxt.text = currPlanet.resourceTotal.ToString();
        MatTransCapTxt.text = currTrans.capacityCurrent.ToString();

    }

    public void MaterialPlanetToTransButton(int place) {
        Transport currTrans = Transports[transport];
        Planet currPlanet = Sectors[currTrans.sector , currTrans.planet];

        if (place == 0) {//men
            if (currPlanet.menTotal > 0 && currTrans.capacityCurrent < currTrans.capacityTotal) {
                currPlanet.menTotal -= 1;
                currTrans.menTotal += 1;
                currTrans.capacityCurrent += 1;
                currPlanet.resourceTotal -= 1;
            }
        }
        else if (place == 1) {//mineral
            if (currPlanet.mineralTotal > 0 && currTrans.capacityCurrent < currTrans.capacityTotal) {
                currPlanet.mineralTotal -= 1;
                currTrans.mineralTotal += 1;
                currTrans.capacityCurrent += 1;
                currPlanet.resourceTotal -= 1;
            }
        }
        else if (place == 2) {//supplies
            if (currPlanet.suppliesTotal > 0 && currTrans.capacityCurrent < currTrans.capacityTotal) {
                currPlanet.suppliesTotal -= 1;
                currTrans.suppliesTotal += 1;
                currTrans.capacityCurrent += 1;
                currPlanet.resourceTotal -= 1;
            }
        }
        else if (place == 3) {//ceramite
            if (currPlanet.ceramiteTotal > 0 && currTrans.capacityCurrent < currTrans.capacityTotal) {
                currPlanet.ceramiteTotal -= 1;
                currTrans.ceramiteTotal += 1;
                currTrans.capacityCurrent += 1;
                currPlanet.resourceTotal -= 1;
            }
        }

        SetMaterialTransTxt();
    }

    public void MaterialTransToPlanetButton(int place) {
        Transport currTrans = Transports[transport];
        Planet currPlanet = Sectors[currTrans.sector , currTrans.planet];

        if (place == 0) {
            if (currTrans.menTotal > 0 && currPlanet.resourceTotal < 20) {
                currTrans.menTotal -= 1;
                currPlanet.menTotal += 1;
                currTrans.capacityCurrent -= 1;
                currPlanet.resourceTotal += 1;
            }
        }
        else if (place == 1) {
            if (currTrans.mineralTotal > 0 && currPlanet.resourceTotal < 20) {
                currTrans.mineralTotal -= 1;
                currPlanet.mineralTotal += 1;
                currTrans.capacityCurrent -= 1;
                currPlanet.resourceTotal += 1;
            }
        }
        else if (place == 2) {
            if (currTrans.suppliesTotal > 0 && currPlanet.resourceTotal < 20) {
                currTrans.suppliesTotal -= 1;
                currPlanet.suppliesTotal += 1;
                currTrans.capacityCurrent -= 1;
                currPlanet.resourceTotal += 1;
            }
        }
        else if (place == 3) {
            if (currTrans.ceramiteTotal > 0 && currPlanet.resourceTotal < 20) {
                currTrans.ceramiteTotal -= 1;
                currPlanet.ceramiteTotal += 1;
                currTrans.capacityCurrent -= 1;
                currPlanet.resourceTotal += 1;
            }
        }

        SetMaterialTransTxt();
    }

    public void SetMaterialTransTxt() {
        Transport currTrans = Transports[transport];
        Planet currPlanet = Sectors[currTrans.sector , currTrans.planet];

        PlanetMenTxt.text = currPlanet.menTotal.ToString();
        TransportMenTxt.text = currTrans.menTotal.ToString();
        PlanetMineralTxt.text = currPlanet.mineralTotal.ToString();
        TransportMineralTxt.text = currTrans.mineralTotal.ToString();
        PlanetSuppliesTxt.text = currPlanet.suppliesTotal.ToString();
        TransportSuppliesTxt.text = currTrans.suppliesTotal.ToString();
        PlanetCeramiteTxt.text = currPlanet.ceramiteTotal.ToString();
        TransportCeramiteTxt.text = currTrans.ceramiteTotal.ToString();
        MatPlanetCapTxt.text = currPlanet.resourceTotal.ToString();
        MatTransCapTxt.text = currTrans.capacityCurrent.ToString();

    }

    public void TransferUnitsButton() {
        Transport currTrans = Transports[transport];
        Planet currPlanet = Sectors[currTrans.sector , currTrans.planet];

        PlanetInfoBar.SetActive(false);
        TransportInfoBar.SetActive(false);
        RegimentInfoBar.SetActive(false);
        TranferMaterialsInfoBar.SetActive(false);
        TransferUnitsInfoBar.SetActive(true);
        RefreshTransferUnitButtons();

        UnitTransPlanetTxt.text = "(" + currTrans.sector + ", " + currTrans.planet + ")";
        UnitTransTransTxt.text = transport.ToString();
        UnitPlanetSpaceTxt.text = currPlanet.resourceTotal.ToString();
        UnitTransSpaceTxt.text = currTrans.capacityCurrent.ToString();
        


    }

    public void RefreshTransferUnitButtons()
    {
        Transport currTrans = Transports[transport];
        Planet currPlanet = Sectors[currTrans.sector, currTrans.planet];

        foreach (GameObject button in TransferUnitPlanetButtons)
        {
            button.SetActive(false);
        }
        foreach (GameObject button in TransferUnitTransButtons)
        {
            button.SetActive(false);
        }

        for(int i = 0; i < currPlanet.units.Count; i++){

            TransferUnitPlanetButtons[i].SetActive(true);
            TransferRegimentPlanetButtons[i].text = currPlanet.units[i].ToString();

        }

        for(int i = 0; i < currTrans.units.Count; i++) {

            TransferUnitTransButtons[i].SetActive(true);
            TransferRegimentTransButtons[i].text = currTrans.units[i].ToString();

        }
        

    }

    public void TransferRegimentPlanetButton(int index) { 

        Transport currTrans = Transports[transport];
        Planet currPlanet = Sectors[currTrans.sector, currTrans.planet];


        if (currTrans.units.Count < 10) { 
            int temp = currPlanet.units[index];
            currPlanet.units.RemoveAt(index);
            currTrans.units.Add(temp);
        }


        RefreshTransferUnitButtons();

    }

    public void TransferRegimentTransButton(int index) {

        Transport currTrans = Transports[transport];
        Planet currPlanet = Sectors[currTrans.sector, currTrans.planet];

        if (currPlanet.units.Count < 10)
        {
            int temp = currTrans.units[index];
            currTrans.units.RemoveAt(index);
            currPlanet.units.Add(temp);
        }

        RefreshTransferUnitButtons();

    }

    public void TransferMaterialCloseButton() {

        TranferMaterialsInfoBar.SetActive(false);

    }

    public void RegimentCloseButton() {//Red X button in the top right of RegimentInfoBar

        RegimentInfoBar.SetActive(false);

    }

    public void PlanetInfoBarCloseButton() {//Red X button in the top right of PlanetInfoBar

        PlanetInfoBar.SetActive(false);

    }

    public void TransportInfoBarCloseButton() {//Red X button in the top right of TransportInfoBar

        TransportInfoBar.SetActive(false);

    }

    public void TransferUnitsCloseButton()
    {
        TransferUnitsInfoBar.SetActive(false);
    }

    public void RecruitRegimentButton(int index) {//Method called whenever a player clicks on the circular sub-buttons in RegimentInfoBar to recruit a regiment
        if (Sectors[sector, planet].menTotal >= 4 && Sectors[sector , planet].mineralTotal >= 4 && Sectors[sector , planet].suppliesTotal >= 4) {

            Sectors[sector , planet].menTotal -= 4;
            Sectors[sector , planet].mineralTotal -= 4;
            Sectors[sector , planet].suppliesTotal -= 4;
            Sectors[sector , planet].resourceTotal -= 12;

            Sectors[sector , planet].units.Add(Sectors[sector , planet].unitValues[index]);
            Sectors[sector , planet].unitValues[index] = -1;
            RegimentInfoBar.SetActive(false);

            Sectors[sector , planet].unitTotal++;
            UnitCapTotalTxt.text = Sectors[sector , planet].unitTotal.ToString();

            if (Sectors[sector , planet].unitTotal >= 10) {
                RegimentBtn.interactable = false;
            }

            int temp = 0;
            for (int i = 0; i < Sectors[sector , planet].units.Count; i++) {
                temp += Sectors[sector , planet].units[i];
            }

            UnitStrengthTxt.text = temp.ToString();

            SetResourceTotals();
        }
    }

    public void Lvl1PlanetActivate() {//called when a player want to activate a lvl 1 planet resource, under PlanetInfoBar

        Sectors[sector , planet].menTotal += Sectors[sector , planet].men[0];
        Sectors[sector , planet].resourceTotal += Sectors[sector , planet].men[0];
        Sectors[sector, planet].tapCooldown = 1;
        PlanetCooldownTxt.text = "1";
        Activate1Btn.interactable = false;
        Activate2Btn.interactable = false;
        Activate3Btn.interactable = false;

        if (Sectors[sector, planet].resourceTotal > 20) {
            int temp = Sectors[sector , planet].resourceTotal - 20;
            Sectors[sector , planet].menTotal -= temp;
            Sectors[sector , planet].resourceTotal -= temp;
        }

        Sectors[sector , planet].mineralTotal += Sectors[sector , planet].minerals[0];
        Sectors[sector , planet].resourceTotal += Sectors[sector , planet].minerals[0];
        if (Sectors[sector , planet].resourceTotal > 20) {
            int temp = Sectors[sector , planet].resourceTotal - 20;
            Sectors[sector , planet].mineralTotal -= temp;
            Sectors[sector , planet].resourceTotal -= temp;
        }

        Sectors[sector , planet].suppliesTotal += Sectors[sector , planet].supplies[0];
        Sectors[sector , planet].resourceTotal += Sectors[sector , planet].supplies[0];
        if (Sectors[sector , planet].resourceTotal > 20) {
            int temp = Sectors[sector , planet].resourceTotal - 20;
            Sectors[sector , planet].suppliesTotal -= temp;
            Sectors[sector , planet].resourceTotal -= temp;
        }

        SetResourceTotals();

    }

    public void Lvl2PlanetActivate() {//called when a player want to activate a lvl 2 planet resource, under PlanetBarInfo

        Sectors[sector , planet].menTotal += Sectors[sector , planet].men[1];
        Sectors[sector , planet].resourceTotal += Sectors[sector , planet].men[1];
        Sectors[sector, planet].tapCooldown = 2;
        PlanetCooldownTxt.text = "2";
        Activate1Btn.interactable = false;
        Activate2Btn.interactable = false;
        Activate3Btn.interactable = false;

        if (Sectors[sector , planet].resourceTotal > 20) {
            int temp = Sectors[sector , planet].resourceTotal - 20;
            Sectors[sector , planet].menTotal -= temp;
            Sectors[sector , planet].resourceTotal -= temp;
        }

        Sectors[sector , planet].mineralTotal += Sectors[sector , planet].minerals[1];
        Sectors[sector , planet].resourceTotal += Sectors[sector , planet].minerals[1];
        if (Sectors[sector , planet].resourceTotal > 20) {
            int temp = Sectors[sector , planet].resourceTotal - 20;
            Sectors[sector , planet].mineralTotal -= temp;
            Sectors[sector , planet].resourceTotal -= temp;
        }

        Sectors[sector , planet].suppliesTotal += Sectors[sector , planet].supplies[1];
        Sectors[sector , planet].resourceTotal += Sectors[sector , planet].supplies[1];
        if (Sectors[sector , planet].resourceTotal > 20) {
            int temp = Sectors[sector , planet].resourceTotal - 20;
            Sectors[sector , planet].suppliesTotal -= temp;
            Sectors[sector , planet].resourceTotal -= temp;
        }

        SetResourceTotals();

    }

    public void Lvl3PlanetActivate() {//called when a player want to activate a lvl 3 planet resource, under PlanetBarInfo

        Sectors[sector , planet].menTotal += Sectors[sector , planet].men[2];
        Sectors[sector , planet].resourceTotal += Sectors[sector , planet].men[2];
        Sectors[sector, planet].tapCooldown = 3;
        PlanetCooldownTxt.text = "3";
        Activate1Btn.interactable = false;
        Activate2Btn.interactable = false;
        Activate3Btn.interactable = false;

        if (Sectors[sector , planet].resourceTotal > 20) {
            int temp = Sectors[sector , planet].resourceTotal - 20;
            Sectors[sector , planet].menTotal -= temp;
            Sectors[sector , planet].resourceTotal -= temp;
        }

        Sectors[sector , planet].mineralTotal += Sectors[sector , planet].minerals[2];
        Sectors[sector , planet].resourceTotal += Sectors[sector , planet].minerals[2];
        if (Sectors[sector , planet].resourceTotal > 20) {
            int temp = Sectors[sector , planet].resourceTotal - 20;
            Sectors[sector , planet].mineralTotal -= temp;
            Sectors[sector , planet].resourceTotal -= temp;
        }

        Sectors[sector , planet].suppliesTotal += Sectors[sector , planet].supplies[2];
        Sectors[sector , planet].resourceTotal += Sectors[sector , planet].supplies[2];
        if (Sectors[sector , planet].resourceTotal > 20) {
            int temp = Sectors[sector , planet].resourceTotal - 20;
            Sectors[sector , planet].suppliesTotal -= temp;
            Sectors[sector , planet].resourceTotal -= temp;
        }

        Sectors[sector , planet].ceramiteTotal += Sectors[sector , planet].ceramite;
        Sectors[sector , planet].resourceTotal += Sectors[sector , planet].ceramite;
        if (Sectors[sector , planet].resourceTotal > 20) {
            int temp = Sectors[sector , planet].resourceTotal - 20;
            Sectors[sector , planet].ceramiteTotal -= temp;
            Sectors[sector , planet].resourceTotal -= temp;
        }

        SetResourceTotals();

    }

    void SetResourceTotals() {//sets the Planet's total resource text to the propper amounts

        TotalResourceTxt.text = Sectors[sector , planet].resourceTotal.ToString();
        TotalMenTxt.text = Sectors[sector , planet].menTotal.ToString();
        TotalMineralTxt.text = Sectors[sector , planet].mineralTotal.ToString();
        TotalSuppliesTxt.text = Sectors[sector , planet].suppliesTotal.ToString();
        TotalCeramiteTxt.text = Sectors[sector , planet].ceramiteTotal.ToString();

    }

    public void PlanetInfoBarCall(int sector, int planet) {//called to display the PlanetInfoBar
        this.sector = sector;
        this.planet = planet;

        PlanetInfoBar.SetActive(true);
        TransportInfoBar.SetActive(false);
        RegimentInfoBar.SetActive(false);

        if (Sectors[sector , planet].unitTotal >= 10) {
            RegimentBtn.interactable = false;
        }
        else {
            RegimentBtn.interactable = true;
        }

        if (Sectors[sector, planet].tapCooldown > 0)
        {
            Activate1Btn.interactable = false;
            Activate2Btn.interactable = false;
            Activate3Btn.interactable = false;
        }
        else
        {
            Activate1Btn.interactable = true;
            Activate2Btn.interactable = true;
            Activate3Btn.interactable = true;
        }

        PlanetNameTxt.text = "Sector: " + sector + " | Planet: " + planet;
        PlanetTypeTxt.text = Sectors[sector , planet].name;
        PlanetCooldownTxt.text = Sectors[sector , planet].tapCooldown.ToString();

        Lvl1MenTxt.text = Sectors[sector , planet].men[0].ToString();
        Lvl1MineralTxt.text = Sectors[sector , planet].minerals[0].ToString();
        Lvl1SuppliesTxt.text = Sectors[sector , planet].supplies[0].ToString();
        Lvl1CeramiteTxt.text = "0";
        Lvl2MenTxt.text = Sectors[sector , planet].men[1].ToString();
        Lvl2MineralTxt.text = Sectors[sector , planet].minerals[1].ToString();
        Lvl2SuppliesTxt.text = Sectors[sector , planet].supplies[1].ToString();
        Lvl2CeramiteTxt.text = "0";
        Lvl3MenTxt.text = Sectors[sector , planet].men[2].ToString();
        Lvl3MineralTxt.text = Sectors[sector , planet].minerals[2].ToString();
        Lvl3SuppliesTxt.text = Sectors[sector , planet].supplies[2].ToString();
        Lvl3CeramiteTxt.text = Sectors[sector , planet].ceramite.ToString();

        RegimentsTxt.text = Sectors[sector , planet].regiments.ToString();
        UnitCapTotalTxt.text = Sectors[sector , planet].unitTotal.ToString();

        int temp = 0;
        for (int i = 0; i < Sectors[sector, planet].units.Count;i++) {
            temp += Sectors[sector , planet].units[i];
        }

        UnitStrengthTxt.text = temp.ToString();

        if (Sectors[sector, planet].manufactorum)
        {
            ManufactorumBtn.interactable = true;
            ManufactorumBtn.GetComponent<Image>().sprite = ManufactorumEnabled;
        }
        else
        {
            ManufactorumBtn.interactable = false;
            ManufactorumBtn.GetComponent<Image>().sprite = ManufactorumDisabled;
        }

        SetResourceTotals();

        if (Sectors[sector , planet].conquered) {
            RegimentBtn.interactable = false;
            ManufactorumBtn.interactable = false;
            Activate1Btn.interactable = false;
            Activate2Btn.interactable = false;
            Activate3Btn.interactable = false;

            PlanetConqueredTxt.text = "C o n q u e r e d";
            PlanetConqueredBackgroundTxt.text = "C o n q u e r e d";
        }
        else {
            RegimentBtn.interactable = true;
            ManufactorumBtn.interactable = true;
            Activate1Btn.interactable = true;
            Activate2Btn.interactable = true;
            Activate3Btn.interactable = true;

            PlanetConqueredTxt.text = "";
            PlanetConqueredBackgroundTxt.text = "";
        }

}

    public void TransportInfoBarCall(int num) {//called to diplay the TransportInfoBar
        PlanetInfoBar.SetActive(false);
        TransportInfoBar.SetActive(true);
        RegimentInfoBar.SetActive(false);

        foreach (GameObject button in TravelButtons) {
            button.SetActive(false);
        }

        transport = num;

        TransportTxt.text = num.ToString();
        TransTotalTxt.text = Transports[transport].capacityCurrent.ToString();
        TransMenTxt.text = Transports[transport].menTotal.ToString();
        TransMineralTxt.text = Transports[transport].mineralTotal.ToString();
        TransSuppliesTxt.text = Transports[transport].suppliesTotal.ToString();
        TransCeramiteTxt.text = Transports[transport].ceramiteTotal.ToString();
        TransUnitsTxt.text = Transports[transport].units.Count.ToString();

        int temp = 0;
        foreach (int i in Transports[transport].units) {
            temp += i;
        }
        TransUnitPowerTxt.text = temp.ToString();

        for (int i = 0; i < Sectors[Transports[num].sector, Transports[num].planet].Connections.Count; i++) {
            Planet currPlanet0 = Sectors[Transports[num].sector , Transports[num].planet];
            int sec = currPlanet0.Connections[i].sector;
            int plan = currPlanet0.Connections[i].planet;

            if (Sectors[sec, plan].hasTransport == false) {
                TravelButtons[i].SetActive(true);
                TravelButtonsTxt[i].text = Sectors[Transports[num].sector , Transports[num].planet].Connections[i].sector + ", " + Sectors[Transports[num].sector , Transports[num].planet].Connections[i].planet;
            }
        }

        Planet currPlanet = Sectors[Transports[num].sector , Transports[num].planet];
        if (currPlanet.conquered) {

            TransferMaterialBtn.interactable = false;
            TransferUnitsBtn.interactable = false;

        }
        else {
            TransferMaterialBtn.interactable = true;
            TransferUnitsBtn.interactable = true;
        }
    }

    public void Travel1Button() {//under TransportInfoBar, move the selected transport to the first planet option
        Planet currPlanet = Sectors[Transports[transport].sector , Transports[transport].planet];
        currPlanet.hasTransport = false;

        int sec = Sectors[Transports[transport].sector , Transports[transport].planet].Connections[0].sector;
        int plan = Sectors[Transports[transport].sector , Transports[transport].planet].Connections[0].planet;

        Transports[transport].sector = sec;
        Transports[transport].planet = plan;
        Transports[transport].SetPosition(Sectors[Transports[transport].sector, Transports[transport].planet]);

        Sectors[Transports[transport].sector , Transports[transport].planet].hasTransport = true;

        TransportInfoBar.SetActive(false);
    }

    public void Travel2Button() {//under TransportInfoBar, move the selected transport to the second planet option
        Planet currPlanet = Sectors[Transports[transport].sector , Transports[transport].planet];
        currPlanet.hasTransport = false;

        int sec = Sectors[Transports[transport].sector , Transports[transport].planet].Connections[1].sector;
        int plan = Sectors[Transports[transport].sector , Transports[transport].planet].Connections[1].planet;

        Transports[transport].sector = sec;
        Transports[transport].planet = plan;
        Transports[transport].SetPosition(Sectors[Transports[transport].sector , Transports[transport].planet]);

        Sectors[Transports[transport].sector , Transports[transport].planet].hasTransport = true;

        TransportInfoBar.SetActive(false);
    }

    public void Travel3Button() {//under TransportInfoBar, move the selected transport to the thrid planet option
        Planet currPlanet = Sectors[Transports[transport].sector , Transports[transport].planet];
        currPlanet.hasTransport = false;

        int sec = Sectors[Transports[transport].sector , Transports[transport].planet].Connections[2].sector;
        int plan = Sectors[Transports[transport].sector , Transports[transport].planet].Connections[2].planet;

        Transports[transport].sector = sec;
        Transports[transport].planet = plan;
        Transports[transport].SetPosition(Sectors[Transports[transport].sector , Transports[transport].planet]);

        Sectors[Transports[transport].sector , Transports[transport].planet].hasTransport = true;

        TransportInfoBar.SetActive(false);
    }

    void SetConnections() {//called in GameController awake method, sets the connections between all the planets. (not complete)

        //Player 1 sector

        //terra - 0, 0
        Sectors[0, 0].AddPlanetConnection(new Pos(0, 1));//to blue
        Sectors[0, 0].AddPlanetConnection(new Pos(0, 2));//to red
        Sectors[0, 0].AddPlanetConnection(new Pos(0, 3));//to green

        //blue - 0, 1
        Sectors[0, 1].AddPlanetConnection(new Pos(0, 0));//to terra

        //red - 0, 2
        Sectors[0, 2].AddPlanetConnection(new Pos(0, 0));//to terra
        Sectors[0, 2].AddPlanetConnection(new Pos(0, 4));//to yellow

        //green - 0, 3
        Sectors[0, 3].AddPlanetConnection(new Pos(0, 0));//to terra
        Sectors[0, 3].AddPlanetConnection(new Pos(0, 4));//to yellow

        //yellow - 0, 4
        Sectors[0, 4].AddPlanetConnection(new Pos(0, 2));//to red
        Sectors[0, 4].AddPlanetConnection(new Pos(0, 3));//to green

        //enemy south
        Sectors[1 , 0].AddPlanetConnection(new Pos(0,1));//to blue

        //enemy north
        Sectors[1 , 1].AddPlanetConnection(new Pos(0, 4));//to yellow
        Sectors[1 , 1].AddPlanetConnection(new Pos(0, 3));//to green

    }
}

public class Pos {//simple class to store the positions of connections for planets

    public int sector;
    public int planet;

    public Pos(int _x, int _y) {
        sector = _x;
        planet = _y;
    }
}
