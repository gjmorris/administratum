﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet {

    //energy 
    //men
    //minerals
    //supplies
    //ceramite
    //regiments to raise
    //manufactorum

    public int tapCooldown = 0;

    public int energy;

    public int[] men = new int[3];
    public int[] minerals = new int[3];
    public int[] supplies = new int[3];
    public int ceramite;

    public int menTotal = 0;
    public int mineralTotal = 0;
    public int suppliesTotal = 0;
    public int ceramiteTotal = 0;
    public int resourceTotal = 0;
    public int unitTotal = 0;

    public List<int> units = new List<int>();

    public int regiments;
    public int[] unitValues = new int[10];

    public string name;

    public bool manufactorum;
    public bool hasTransport = false;
    public bool conquered = false;

    public Vector3 transportPosition;
    public Vector3 transportRotation;

    public List<Pos> Connections = new List<Pos>(); 

    public void Initialize(string type, int _energy, Vector3 transPos, Vector3 transRot)
    {
        energy = _energy;
        name = type;

        transportPosition = transPos;
        transportRotation = transRot;

        if (type == "Men") {

            men[0] = Random.Range(1 , 4);
            men[1] = Random.Range(3 , 6);
            men[2] = Random.Range(4 , 8);

            minerals[0] = 0;
            minerals[1] = Random.Range(0 , 2);
            minerals[2] = Random.Range(0 , 3);

            supplies[0] = 0;
            supplies[1] = Random.Range(0 , 2);
            supplies[2] = Random.Range(0 , 3);

            if (Random.Range(0 , 10) == 9) {
                ceramite = 1;
            }

            manufactorum = false;
            regiments = 6;

        }
        else if (type == "Minerals") {

            men[0] = 0;
            men[1] = Random.Range(0 , 2);
            men[2] = Random.Range(0 , 3);

            minerals[0] = Random.Range(1 , 4);
            minerals[1] = Random.Range(3 , 6);
            minerals[2] = Random.Range(4 , 8);

            supplies[0] = 0;
            supplies[1] = Random.Range(0 , 2);
            supplies[2] = Random.Range(0 , 3);

            int tempRand = Random.Range(0 , 10);

            if (tempRand >= 7) {
                ceramite = 1;
            }
            else if (tempRand == 1 || tempRand == 2) {
                ceramite = 2;
            }


            manufactorum = false;
            regiments = 4;

        }
        else if (type == "Supplies") {
            men[0] = 0;
            men[1] = Random.Range(0 , 2);
            men[2] = Random.Range(0 , 3);

            minerals[0] = 0;
            minerals[1] = Random.Range(0 , 2);
            minerals[2] = Random.Range(0 , 3);

            supplies[0] = Random.Range(1 , 4);
            supplies[1] = Random.Range(3 , 6);
            supplies[2] = Random.Range(4 , 8);

            if (Random.Range(0 , 10) == 9) {
                ceramite = 1;
            }

            if (Random.Range(0 , 5) >= 3) {//3
                manufactorum = true;
            }
            else {
                manufactorum = false;
            }

            regiments = 4;
        }
        else if (type == "Balanced") {
            men[0] = 0;
            men[1] = Random.Range(0 , 2);
            men[2] = Random.Range(0 , 3);

            minerals[0] = 0;
            minerals[1] = Random.Range(0 , 2);
            minerals[2] = Random.Range(0 , 3);

            supplies[0] = Random.Range(1 , 4);
            supplies[1] = Random.Range(3 , 6);
            supplies[2] = Random.Range(4 , 8);

            if (Random.Range(0 , 10) >= 8) {
                ceramite = Random.Range(1 , 3);
            }

            if (Random.Range(0 , 5) >= 4) {//4
                manufactorum = true;
            }
            else {
                manufactorum = false;
            }

            regiments = 5;
        }
        else if (type == "Terra") {
            name = "Holy Terra";

            men[0] = Random.Range(1 , 4);
            men[1] = Random.Range(3 , 6);
            men[2] = Random.Range(4 , 8);

            minerals[0] = Random.Range(1 , 4);
            minerals[1] = Random.Range(3 , 6);
            minerals[2] = Random.Range(4 , 8);

            supplies[0] = Random.Range(1 , 4);
            supplies[1] = Random.Range(3 , 6);
            supplies[2] = Random.Range(4 , 8);

            ceramite = Random.Range(2 , 4);

            manufactorum = true;
            regiments = 7;

        }
        else if (type == "enemy") {
            name = "Vortex";

            conquered = true;
        }


    }

    public void AddPlanetConnection(Pos planet) {

        Connections.Add(planet);

    }

    public void ReduceCooldown()
    {

        tapCooldown--;
        if (tapCooldown <= 0)
        {
            tapCooldown = 0;
        }

    }

    

    

}
