﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetListener : MonoBehaviour {

    public int sector;
    public int planet;

    private void OnMouseDown() {

        GameController.gc.PlanetInfoBarCall(sector, planet);

    }
}
