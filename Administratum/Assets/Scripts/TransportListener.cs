﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransportListener : MonoBehaviour {

    public int num;

    public void Initialize(int num) {
        this.num = num;
    }

    private void OnMouseDown() {

        GameController.gc.TransportInfoBarCall(num);

    }
}
