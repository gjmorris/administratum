﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy  {

    public int number;

    public int sector;
    public int planet;

    public GameObject ship;

    public int strength;

    public Enemy(int num, int sect, int plnt, GameObject ship) {

        number = num;
        sector = sect;
        planet = plnt;

        this.ship = ship;

        ship.GetComponent<EnemyListener>().Initialize(number);


    }

    public void SetPosition(Planet p) {

        this.ship.transform.position = p.transportPosition;
        this.ship.transform.rotation = Quaternion.Euler(p.transportRotation);

    }

}
